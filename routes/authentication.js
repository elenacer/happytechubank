const express = require('express');
const router = express.Router();
const requestJson = require('request-json');
const crypt = require('../crypt');

const baseMLabURL = process.env.MLAB_BASE_URL;
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

/* POST user login. */
router.post('/', function(req, res, next) {
  console.log("POST /happytechubank/v1/login");
  var query = "q=" + JSON.stringify({"email":req.body.email});
  console.log("query es " + query);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function(errGet, resMlabGet, bodyGet){
      if (errGet) {
        console.log(resMlabGet);
        var response = {
          "msg" : "Internal error."
        }
        res.status(500).send(response);
      } else {
          if (bodyGet.length > 0){
            var user = bodyGet[0];
            console.log("La psw del usuario sigue siendo " + req.body.password);
            var autorizado = crypt.checkPassword(req.body.password, user.password);
            console.log("autorizado " + autorizado);
            if(autorizado){
              var data = {"$set":{"logged":true}};
              httpClient.put("user?" + query + "&" + mLabAPIKEY, data,
                function(errPut, resMlabPut, bodyPut){
                  if (errPut) {
                    var response = {
                      "msg" : "Internal error."
                    }
                    res.status(500).send(response);
                  } else {
                      console.log(bodyPut);
                      var response = {
                        "msg" : "Usuario logado",
                        "id" : user._id.$oid,
                        "first_name" : user.first_name
                      }
                      res.send(response);
                  }
                }
              )
            } else {
                var response = {
                  "msg" : "Usuario no autorizado"
                }
                res.status(401).send(response);
            }
          } else {
              var response = {
                "msg" : "Usuario no autorizado"
              }
              res.status(403).send(response);
          }
        }
    }
  )
});

/* POST user logout. */
router.post('/:id', function(req, res, next) {
  console.log(req.params);
  console.log("POST /happytechubank/v1/logout/"+req.params.id);

  console.log("El id del usuario a hacer logout es "+ req.params.id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var id = req.params.id;

  console.log("El id del usuario a buscar es "+ id);
  var query = "q=" + JSON.stringify({"_id": {"$oid": id}, "logged":true});
  console.log("Query es "+ query);

  httpClient.put("user?" + query + "&" + mLabAPIKEY, {"$unset":{"logged":""}},
    function(errPut, resMLabPut, bodyPut) {
      var response = {
        "msg" : "Logout incorrecto"
      }
      console.log(bodyPut);
      res.status(400);
      if (errPut){
        res.status(500);
      } else {
        if (bodyPut.n > 0){
          console.log("Usuario actualizado en Mlab");
          var response = {
            "msg" : "Logout correcto",
             "idUsuario" : id
          }
          res.status(200);
        } else {
          console.log("Usuario no encontrado");
        }
      }
      res.send(response);
    }
  )


});

module.exports = router;
