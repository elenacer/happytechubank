const express = require('express');
const router = express.Router();
const requestJson = require('request-json');

const baseMLabURL = process.env.MLAB_BASE_URL;
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

/* GET transactions listing. */
router.get('/', function(req, res, next) {
  console.log("GET /happytechubank/v1/transactions");

  console.log(baseMLabURL);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("transaction?" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (!err){
        var response = {
          "size" : body.length,
          "result" : body
        }
      } else {
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
      }
      res.send(response);
    }
  );
});

/* GET transaction by Account_Id listing. */
router.get('/:account_id', function(req, res, next) {
  console.log("GET /happytechubank/v1/transactions/"+req.params.account_id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var account_id = req.params.account_id;
  console.log("El id de la cuenta a buscar es "+ account_id);
  var query_filters = {"account.account_id": account_id};

  // procesamos posibles filtros que vengan por query url
  console.log("Parámetros recibidos:");
  console.log(req.query);

  // var currency_filter = req.query.currency;
  // if (currency_filter){
  //   query_filters.amount = {"$match" : {"currency" : currency_filter}};
  // }
  var money_flow_filter = req.query.money_flow;
  if (money_flow_filter){
    query_filters.money_flow = money_flow_filter;
  }

  // var from_operation_date_filter = req.query.from_operation_date;
  // var until_operation_date_filter = req.query.until_operation_date;
  // if (from_operation_date_filter && until_operation_date_filter){
  //   query_filters.operation_date = { "$and": [{"operation_date": {"$gte": {"$date": from_operation_date_filter}, "$lte": {"$date": until_operation_date_filter}}} ] };
  //
  // }else {
  //   if (from_operation_date_filter){
  //     query_filters.operation_date = { "$gte" : {"$date": from_operation_date_filter}};
  //   }
  //   if (until_operation_date_filter){
  //     query_filters.operation_date = { "$lte" : {"$date": until_operation_date_filter}};
  //   }
  // }

  var query = "q=" + JSON.stringify(query_filters);
  console.log("Query es "+ query);

  var order = "s=" + JSON.stringify({"last_name": -1})

  httpClient.get("transaction?" + query + "&" + order + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        console.log(err);
        var response = {
          "msg" : "Error obteniendo movimientos"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = {
            "size" : body.length,
            "result" : body
          }
        } else {
          var response = {
            "msg" : "Cuenta no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
});

/* GET currencies by Account_Id listing. */
router.get('/:account_id/currencies', function(req, res, next) {
  console.log("GET /happytechubank/v1/transactions/"+req.params.account_id+"/currencies");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var account_id = req.params.account_id;
  console.log("El id de la cuenta a buscar es "+ account_id);
  var query_filters = {"account.account_id": account_id};
  var query = "q=" + JSON.stringify(query_filters);
  console.log("Query es "+ query);

  httpClient.get("transaction?" + query + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo divisas"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var divisas = [] ;
          var result = [] ;
          body.forEach(function(transaction) {
            if (!divisas.includes(transaction.amount.currency)){
              console.log("Nueva divisa encontrada "+transaction.amount.currency);
              divisas.push(transaction.amount.currency);
              result.push({"currency":transaction.amount.currency});
            }
          });
          var response = {
            "size" : result.length,
            "result" : result
          }
        } else {
          var response = {
            "msg" : "Divisas no encontradas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
});

/* POST transaction create. */
router.post('/', function(req, res, next) {
  console.log("POST /happytechubank/v1/transactions");
  console.log(req.body);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("transaction?" + mLabAPIKEY, req.body,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo divisas"
        }
        res.status(500);
      }else {
        console.log("Transacción creada en Mlab");
        var account_id = body.account.account_id;
        //procesamos el id para evitar la inyección SQL
        console.log("El id de la cuenta a buscar es "+ account_id);
        var query = "q=" + JSON.stringify({"_id": {"$oid": account_id}});
        console.log("Query es "+ query);

        httpClient.get("account?" + query + "&" + mLabAPIKEY,
          function(errGet, resMLabGet, bodyGet) {
            if (errGet){
              var response = {
                "msg" : "Error obteniendo cuenta"
              }
              res.status(500);
            } else{
              if (bodyGet.length > 0){
                console.log(bodyGet);
                var account = bodyGet[0];
                if (req.body.money_flow == "INCOME"){
                  console.log("Abono");
                  account.amount.amount = account.amount.amount + req.body.amount.amount;
                  console.log(account.amount.amount);
                }else {
                  console.log("Gasto");
                  account.amount.amount = account.amount.amount - req.body.amount.amount;
                  console.log(account.amount.amount);
                }
                httpClient.put("account?" + query + "&" + mLabAPIKEY, account, 
                  function(err, resMLab, body) {
                    console.log("Cuenta actualizada en Mlab");
                    res.status(201).send({"msg":"Cuenta actualizada"});
                  }
                )
              } else {
                res.status(404).send({"msg":"Cuenta no encontrada"});
              }
            }
          }
        )
      }

    }
  )
});

/* DELETE transaction delete. */
router.delete('/:id', function(req, res, next) {
  console.log("DELETE /happytechubank/v1/transactions");
  res.send('respond with a resource');
});

module.exports = router;
