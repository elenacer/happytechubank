const express = require('express');
const router = express.Router();
const requestJson = require('request-json');
const ibanGenerator = require('iban-generator');


const crypt = require('../crypt');

const baseMLabURL = process.env.MLAB_BASE_URL;
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;
const baseGoogleURL = process.env.GOOGLE_BASE_URL;
const googleAPIKEY = "key=" + process.env.GOOGLE_API_KEY;


var chars={
  "á":"a", "é":"e", "í":"i", "ó":"o", "ú":"u",
  "à":"a", "è":"e", "ì":"i", "ò":"o", "ù":"u", "ñ":"n",
  "Á":"A", "É":"E", "Í":"I", "Ó":"O", "Ú":"U",
  "À":"A", "È":"E", "Ì":"I", "Ò":"O", "Ù":"U", "Ñ":"N"}
var expr=/[áàéèíìóòúùñ]/ig;

/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log("GET /happytechubank/v1/users");

  console.log(baseMLabURL);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (!err){
        var response = {
          "size" : body.length,
          "result" : body
        }
      } else {
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
      }
      res.send(response);
    }
  );
});

/* GET user by Id listing. */
router.get('/:user_id', function(req, res, next) {
  console.log("GET /happytechubank/v1/users/"+req.params.user_id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var user_id = req.params.user_id;
  console.log("El id del usuario a buscar es "+ user_id);
  var query = "q=" + JSON.stringify({"_id": {"$oid": user_id}});
  console.log("Query es "+ query);

  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = {
            "size" : body.length,
            "result" : body
          }
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
});

/* POST user create. */
router.post('/', function(req, res, next) {
  console.log("POST /happytechubank/v1/users");
  console.log("email");
  console.log(req.body.email);
  var query = "q=" + JSON.stringify({"email":req.body.email});
  console.log(req.body.email);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKEY,
    function(errGet, resMlabGet, bodyGet){
      if (errGet) {
        var response = {
          "msg" : "Internal error."
        }
        res.status(500).send(response);
      } else {
          if (bodyGet.length > 0){
            console.log("Usuario encontrado");
            var response = {
              "msg" : "Error en el alta"
            }
            res.status(409).send(response);
          } else {
//////////////////////////////////


              var newUser = {
                "email": req.body.email,
                "first_name": req.body.first_name,
                "last_name": req.body.last_name,
                "mother_name": req.body.mother_name,
                "password": crypt.hash(req.body.password),
                "document_type": req.body.document_type,
                "document_number": req.body.document_number,
                "address": req.body.address
              }
              var httpClient2 = requestJson.createClient(baseGoogleURL);
              console.log("otro Cliente http creado");
              var calle = newUser.address.via.replace(expr,function(e){return chars[e]});
              var dir = calle + "+" + newUser.address.numero + "+" + newUser.address.poblacion;
              var miQuery = "json?query=" +dir+ "&" + googleAPIKEY;
              console.log(miQuery);
              console.log(baseGoogleURL+miQuery);
              httpClient2.get(miQuery,
                function(errGoogle, resGoogle, bodyGoogle) {
                  if (errGoogle){
                    console.log("error dir");
                    console.log(bodyGoogle)
                    res.status(500).send({ "msg" : "Error interno"});
                  } else{

                    /////ini else
                    //console.log("dir ok");
                    //console.log(bodyGoogle);
                    var googleRes = bodyGoogle.results;
                    //console.log(res);
                    var dirValidada=false;
                    for (var j=0;j<googleRes.length;j++){
                      //console.log("entra al for");
                      //console.log(googleRes[j].formatted_address);
                      var dirGoogle= googleRes[j].formatted_address;
                      var direccion= dirGoogle.replace(expr,function(e){return chars[e]});
                      direccion= direccion.toLowerCase();
                      calle= calle.toLowerCase();
                      /*
                      console.log(calle);
                      console.log(direccion);
                      console.log(direccion.indexOf(calle));
                      console.log(direccion.indexOf(newUser.address.numero));
                      console.log(direccion.indexOf(newUser.address.postal_code));
                      */
                      if (direccion.indexOf(calle) > -1 && direccion.indexOf(newUser.address.numero) > -1 && direccion.indexOf(newUser.address.postal_code) > -1) {
                        dirValidada=true;
                        break;
                      }
                    }
                    console.log(dirValidada);
                    if (!dirValidada){
                      res.status(409).send({ "msg" : "Dirección no valida"});
                    } else{
                      console.log("creo el user");
                      console.log(newUser);
                      var iban = 'ES' + ibanGenerator.randomNumber();
                      console.log(iban);
                      httpClient.post("user?" + mLabAPIKEY, newUser,
                        function(err, resMlab, body) {
                          //console.log("Usuario creado");
                          console.log(body);
                          if (err){
                            console.log("error");
                            res.status(500).send({ "msg" : "Error interno"});
                          } else{
                            console.log("usuario creado en mLab");
                            var newAccount = {
                              "account_number": iban,
                               "amount": {
                                   "amount": 0,
                                   "currency": "EUR"
                               },
                               "user_id": body._id.$oid
                            }
                            httpClient.post("account?" + mLabAPIKEY, newAccount,
                              function(err, resMlab, body) {
                                if (err){
                                  console.log("error");
                                  res.status(500).send({ "msg" : "Error interno"});
                                } else{
                                  //console.log("cuenta creada en mLab");
                                  res.status(201).send({ "msg" : "Cuenta creada correctamente"});
                                }
                              }
                            )
                          }
                        }
                      )
                      //res.status(201).send(response);

                    }
                    /////fin else
                  }
                }
              )
          }
        }
    }
  )
});
/* DELETE user delete. */
router.delete('/:id', function(req, res, next) {
  console.log("DELETE /happytechubank/v1/users");
  res.send('respond with a resource');
});

module.exports = router;
