const express = require('express');
const router = express.Router();
const requestJson = require('request-json');
const ibanGenerator = require('iban-generator');

const baseMLabURL = process.env.MLAB_BASE_URL;
const mLabAPIKEY = "apiKey=" + process.env.MLAB_API_KEY;

/* GET accounts listing. */
router.get('/', function(req, res, next) {
  console.log("GET /happytechubank/v1/accounts");

  console.log(baseMLabURL);
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (!err){
        var response = {
          "size" : body.length,
          "result" : body
        }
      } else {
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
      }

      res.send(response);
    }
  );
});

/* GET account by User_Id listing. */
router.get('/:user_id', function(req, res, next) {
  console.log("GET /happytechubank/v1/accounts/"+req.params.user_id);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var user_id = req.params.user_id;
  console.log("El id del usuario a buscar es "+ user_id);
  var query = "q=" + JSON.stringify({"user_id": user_id});
  console.log("Query es "+ query);

  httpClient.get("account?" + query + "&" + mLabAPIKEY,
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0){
          var response = {
            "result" : body,
            "size" : body.length
          }
        } else {
          var response = {
            "msg" : "Cuenta no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  );
});

/* PUT account update. */
router.put('/:account_id', function(req, res, next) {
  console.log("GET /happytechubank/v1/accounts/"+req.params.user_id);
  console.log(req.body);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  //procesamos el id para evitar la inyección SQL
  var accout_id = req.params.user_id;
  console.log("El id de la cuenta a buscar es "+ account_id);
  var query = "q=" + JSON.stringify({"_id": {"$oid": account_id}});
  console.log("Query es "+ query);

  httpClient.put("account?" + query + "&" + mLabAPIKEY, req.body,
    function(err, resMLab, body) {
      console.log("Cuenta actualizada en Mlab");
      res.status(201).send({"msg":"Cuenta actualizada"});
    }
  )
});

/* POST account create. */
router.post('/', function(req, res, next) {
  console.log("POST /happytechubank/v1/accounts");
  var iban = 'ES' + ibanGenerator.randomNumber();
  console.log(iban);
  console.log(req.body.id);
  var newAccount = {
    "account_number": iban,
     "amount": {
         "amount": 0,
         "currency": "EUR"
     },
     "user_id": req.body.id
  }
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.post("account?" + mLabAPIKEY, newAccount,
    function(err, resMlab, body) {
      if (err){
        console.log("error");
        res.status(500).send({ "msg" : "Error interno"});
      } else{
        console.log("usuario creado en mLab");
        res.status(201).send({ "msg" : "Cuenta creada correctamente"});
      }
    }
  )
});

/* DELETE account delete. */
router.delete('/:account_id', function(req, res, next) {
  console.log("DELETE /happytechubank/v1/accounts");

  //procesamos el id para evitar la inyección SQL
  var account_id = req.params.account_id;
  console.log("El id de la cuenta a buscar es "+ account_id);
  var query = "q=" + JSON.stringify({"_id": {"$oid" : account_id}});
  console.log("Query es "+ query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.put("account?" + query + "&" + mLabAPIKEY, [{}],
    function(err, resMLab, body) {
      if (err){
        var response = {
          "msg" : "Error borrando cuenta"
        }
        res.status(500);
      } else {
        if (body.removed > 0){
          var response = {
            "msg" : "Cuenta borrada"
          }
        } else {
          var response = {
            "msg" : "Cuenta no encontrada"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
});

module.exports = router;
