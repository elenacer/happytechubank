
console.log("Hola Mundo");
require('dotenv').config();
const createError = require('http-errors');
const express = require('express');

var usersRouter = require('./routes/users');
var accountsRouter = require('./routes/accounts');
var transactionsRouter = require('./routes/transactions');
var authenticationRouter = require('./routes/authentication');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// enable cross domain
app.use(function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
});

app.use('/happytechubank/v1/users', usersRouter);
app.use('/happytechubank/v1/accounts', accountsRouter);
app.use('/happytechubank/v1/transactions', transactionsRouter);
app.use('/happytechubank/v1/login', authenticationRouter);
app.use('/happytechubank/v1/logout', authenticationRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
